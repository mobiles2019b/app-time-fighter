package com.example.time_fighter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var wellcomeMessageTextView: TextView
    private lateinit var wellcomeSubtitleTextView: TextView
    private lateinit var changeTextBoton : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        wellcomeMessageTextView= findViewById(R.id.wellcome_message)
        wellcomeSubtitleTextView = findViewById(R.id.wellcome_subtattile)
        changeTextBoton = findViewById(R.id.check_text_button)

        changeTextBoton.setOnClickListener { changeMessageAndSubtitle() }
    }

    private fun changeMessageAndSubtitle(){
        wellcomeMessageTextView.text = getString(R.string.new_wellcome)
        wellcomeSubtitleTextView.text= getString(R.string.new_subtitle)
    }
}
